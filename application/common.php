<?php
use think\Db;



function jiami($str=''){
    $data=md5($str);
    return $data;
}
//登录
function getadmininfo(array $array){
    $dataaa=Db::table('hd_admin')->where($array)->find();
    return $dataaa;
}

//入库操作
function getinsertinfo($away,$data)
{
        $time = date('Y-m-d H:i:s');
        $data['time'] = strtotime($time);
        $dataa = $away->insert($data);
        return $dataa;
}

//查询数据库
function getfindinfor($away,$fin){
    $awa=$away->where($fin)->find();
    return $awa;
}


//数据库多项查询
function getselectinfor($away,$data){
    $data=$away->where($data)->select();
    return $data;
}


//排序asc   升续
function getascinfor($data,$tiaojian){
    $da=$data->order('$tiaojian asc');
    return $da;
}

//降续desc
function getdescinfor($data,$tiaojian){
    $da=$data->order('$tiaojian asc');
    return $da;
}

//更新数据
function getupdate($away,$data,$wh){
    $data=$away->where($wh)->update($data);
    return $data;
}

//分页
function fenye($datac){
    $count=count($datac);
    $pageid=input('pageid',1,'int');

    //每页10条数据
    $per_page =2;
    //当前页
    $page['nowpage']=$pageid;
    //上一页
    $page['prev']=$pageid-1;
    //下一页
    $page['next'] = $pageid+1;

    //总页数
    $page['total'] = ceil($count/$per_page);
    //判断页码范围
    if($page['prev']<=0){
        $page['prev']=1;
    }
    if($page['next']>=$page['total']){
        $page['next']=$page['total'];
    }
    return $page;
}