<?php
namespace app\admin\controller;
use think\Controller;
use think\Request;
use think\Session;
use think\Db;

class Set extends Controller
{
    public function ie(){
        return view();
    }
    public function register(){
        return view();
    }
    public function setting_user(){
        if(request()->isPost()){
            $name=input('post.name');                              //搜索功能
            if(empty($name)){
                echo "<script>alert('不能为空！');location.href='setting_user.html'</script>";
            }else{
                $data['zname']=$name;
                $ddaa=Db::table('hd_admin')->where($data)->select();
                if(empty($ddaa)){
                    echo "<script>alert('用户不存在！');location.href='setting_user.html'</script>";
                }else{
                    $this->assign('list',$ddaa);
                    return $this->fetch('setting_user');
                }
            }
        }else{
            $list=Db::table('hd_admin')->select();
            $this->assign('list',$list);

            //删除用户
            $id=input('id');
            if(Db::table('hd_admin')->where(array('id' => $id))->delete()){
                echo "<script>alert('删除成功！');location.href='setting_user.html'</script>";
            }
            return $this->fetch();
        }
    }
    public function newinfo(){
        if(request()->isPost()){
            $name=input('post.name');
            $zname=input('post.zname');
            $section=input('post.department');
            $data['name']=$name;

            if(empty($name) and empty($zname) and empty($section)){
                echo "<script>alert('用户名，姓名，所属部门不能为空！');location.href='setting_user.html'</script>";
            }else{
                $ddaa=Db::table('hd_admin')->where($data)->find();
                $wh['id']=$ddaa['id'];
                if(empty($ddaa)){
                    echo "<script>alert('用户不存在');location.href='setting_user.html'</script>";
                }else{
                    $data['zname']=$zname;
                    $data['section']=$section;
                    $aarr = Db::table('hd_admin')->where($wh)->update($data);
                    if(!empty($aarr)){
                        //$this->redirect('index/index',array('修改成功'),2, '页面跳转中...');
                       echo "<script>alert('修改成功！');location.href='setting_user.html'</script>";
                    } else {
                        echo "<script>alert('修改失败！请重新上传');location.href='setting_user.html'</script>";
                    }
                }
            }

        }
        $id=input('id');
        $data['id']=$id;
        $list=Db::table('hd_admin')->where($data)->find();
        $this->assign('list',$list);
        return $this->fetch();
    }
    public function modify_password(){
        if(request()->isPost()){
            $str=session::get('name');


            $psd=input('post.password');
            $newpsd=input('post.newpassword');
            $newpsd1=input('post.newpassword1');
            if(empty($psd) and empty($newpsd) and empty($newpsd1)){
                echo "<script>alert('密码不能为空！');location.href='modify_password.html'</script>";
            }else{
                if($newpsd===$newpsd1){
                    $data['name']=$str;
                    $ddaa=Db::table('hd_admin')->where($data)->find();
                    //判断密码输入的是否正确
                    if(md5($psd)===$ddaa['password']){
                        $wh['password']=md5($newpsd);
                        $aarr = Db::table('hd_admin')->where($data)->update($wh);
                        if(empty($aarr)){
                            echo "<script>alert('密码更新失败！');location.href='modify_password.html'</script>";
                        }else{
                            echo "<script>alert('密码修改成功！');location.href='modify_password.html'</script>";
                        }
                    }else{
                        echo "<script>alert('密码错误，请重新输入！');location.href='modify_password.html'</script>";
                    }
                }else{
                    echo "<script>alert('密码不同！');location.href='modify_password.html'</script>";
                }
            }


        }
        return view();
    }
}