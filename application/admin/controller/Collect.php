<?php
namespace app\admin\controller;

use Symfony\Component\Yaml\Tests\DumperTest;
use think\Controller;
use think\Request;
use think\Session;
use think\Db;
use think\table;
use app\admin\controller\Common;

class Collect extends Common
{
    public function collection()
    {
       
        
        $str = session::get('name');
        $admin['name'] = $str;
        $power = Db::table('hd_admin')->field('power')->where($admin)->find();
        $this->assign('power', $power);
        if (request()->isGet()) {
            $a = input('get.');
            unset($a['page']);
              foreach ($a as $k => $v) {
              $c='%'.$v.'%' ; 
              $b=['like',$c];
              $a[$k]=$b;
             
              if($a[$k][1]=='%%'){
                  unset($a[$k]);
              }
          
          }
       $a['isnew']=1;
       $a['isdelete']=0;
      
  

            $list=Db::table('hd_connection')
            ->alias('a')
            ->join('hd_state b','a.id=b.cid') 
            ->where($a)
            ->order('a.id desc')
            ->paginate(15,false,['query'=>request()->param()]);
              
$datac=$list->toArray();


            //转化图片路径
            foreach ($datac['data'] as $k => $v) {
              
                $img = $v['image'];
               
                $a = explode(',', $img);
                $stt='_';
                $a=substr_replace($a,$stt,9,0);
                $datac['data'][$k]['image2'] = $a;
               
            }
        
            //获取搜索条件
            $data=input('get.');
          
            //传参
            $this->assign('get',$data);
            $this->assign('list',$list->render());
            $this->assign('datac', $datac['data']);
            //渲染图层
            return $this->fetch();
        } else {
           
        }

    }

    
    
    
    
    
    
    public function delete(){
       
     
        $id=input('id');
        if (empty($id)) {
        } else {
            $wh['id'] = $id;
          
            $ttt = Db::table('hd_connection')->where($wh)->update(['isdelete' => 1]);
        if($ttt==''){
            $this->error('删除失败,请重试',url('Collect/collection'));
        }else{
            $this->success('删除成功',url('Collect/collection'));
            }
        }
    }

    public function showInfo()
    {
        $id = input('id');


        $data = Db::table('hd_connection')->where('id=' . $id)->find();

        $data['time'] = date('Y-m-d h:m:s', $data['time']);
        if ($data['lsrtime'] != '') {
            $data['lsrtime'] = date('Y-m-d h:m:s', $data['lsrtime']);
        }
        if ($data['lsctime'] != '') {
            $data['lsctime'] = date('Y-m-d h:m:s', $data['lsctime']);
        }
        $a=explode(',', $data['image']);
        //    dump($a);die;
        foreach ($a as $k=>$v){
            $stt='_';
            $b=substr_replace($v,$stt,9,0);

            $a[$k]=$b;
        }
        $data['image']=$a;
        $this->assign('data', $data);

        return view();
    }

    public function log()
    {

        return view();
    }

    public function comment()
    {
        $id = input('id');
        if (request()->isPost()) {
            $name = Session::get('name');
            $dat = input('post.comment');
            $id = input('post.id');
            $str = input('post.zname');
            $time=input('post.time');
            
            $data['zname'] = $str;
            $data['cid'] = $id;
            $data['name'] = $name;
            $data['time'] = strtotime($time);
            if (empty($dat) || empty($data['zname'])) {
                echo "<script>alert('点评内容，点评人不能为空！');location.href='collection.html'</script>";
            } else {

                $dd = Db::table('hd_comment')->where($data)->find();
                if ($dd == '') {

                    $data['comment'] = $dat;
                 
                    $add = Db::table('hd_comment')->insert($data);
                    if (!empty($add)) {
                        $this->success('点评成功', url('collect/collection'), 2);
                    } else {
                        $this->error('评论失败', url('collect/collection'), 2);
                    }
                } else {
                    $this->error('已评论', url('collect/collection'), 2);
                }
            }
        }

        $dd = Db::table('hd_comment')->where('cid', $id)->select();
        $this->assign('alist', $dd);
        $this->assign('id', $id);
        return view();
    }

    public function liulan()
    {
        $id = input('id');

        $dd = Db::table('hd_comment')->where('cid', $id)->order('time desc')->select();
        $this->assign('alist', $dd);
        return view();
    }

    public function xiugai()
    {
        $id = input('id');
        $comment = Db::table('hd_comment')->field('id,comment,zname,time')->where('id=' . $id)->find();

        $this->assign('comment', $comment);
        if (request()->isPost()) {
            $data = input('post.');
           $data['time']=strtotime($data['time']);

            if ($data['comment'] == '') {
                echo "<script>alert('点评内容不能为空！');location.href='collection.html'</script>";
            } else {
                
                $add = Db::table('hd_comment')->where('id=' . $data['id'])->update($data);
                if (!empty($add)) {
                    $this->success('修改点评成功', url('collect/collection'), 2);
                } else {
                    $this->error('修改点评失败,请重试', url('collect/collection'), 2);
                }
            }
        }


        $dd = Db::table('hd_comment')->where('cid', $id)->select();
      
        $this->assign('alist', $dd);
        $this->assign('id', $id);
        return view();
    }

    
    public function daochu(){
        header("Content-type: text/html; charset=utf-8");
        include'./phpexcel/PHPExcel.php';
        include './phpexcel/phpExcel/Writer/Excel2007.php';
        include './phpexcel/phpExcel/Writer/Excel5.php';
        include './phpexcel/phpExcel/IOFactory.php';
        $objPHPExcel = new \ PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Maarten Balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");
    
        //设置标题
        //    $objPHPExcel->getActiveSheet()->setTitle($filename);
        //设置表头
        $key1 = 1;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$key1, '拍品编号')
        ->setCellValue('B'.$key1, '委托单位')
        ->setCellValue('C'.$key1, '征集人')
        ->setCellValue('D'.$key1, '作者')
        ->setCellValue('E'.$key1, '名称')
        ->setCellValue('F'.$key1, '尺寸')
        ->setCellValue('G'.$key1, '分类')
        ->setCellValue('H'.$key1, '委托人')
        ->setCellValue('I'.$key1, '年代')
        ->setCellValue('J'.$key1, '释文')
        ->setCellValue('K'.$key1, '题跋')
        ->setCellValue('L'.$key1, '款识')
        ->setCellValue('M'.$key1, '钤印')
        ->setCellValue('N'.$key1, '材质')
        ->setCellValue('O'.$key1, '登记时间')
        ->setCellValue('P'.$key1, '备注')
        ->setCellValue('Q'.$key1, '临时入库时间')
        ->setCellValue('R'.$key1, '临时出库时间');
        //设置样式：
        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true); //多个单元格
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);  //列宽必须单个设置
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(false);
        $del_id=input('get.id_str');
        $id=explode(',',$del_id);
        $del_num=count($id);
        for($i=0;$i<$del_num;$i++) {                                       //获取每一个id值，对其进行导出
            $wh['id'] = $id[$i];
            $datac[] = Db::table('hd_connection')->where($wh)->find();
           
           
          
        }
       
        foreach($datac as $key =>$value){
            $key1=$key+2;
            if($value['lsrtime']==''&&$value['lsctime']==''){
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$key1, $value['num'])
                ->setCellValue('B'.$key1, $value['user'])
                ->setCellValue('C'.$key1, $value['zman'])
                ->setCellValue('D'.$key1, $value['author'])
                ->setCellValue('E'.$key1, $value['name'])
                ->setCellValue('F'.$key1, $value['size'])
                ->setCellValue('G'.$key1, $value['type'])
                ->setCellValue('H'.$key1, $value['wtr'])
                ->setCellValue('I'.$key1, $value['years'])
                ->setCellValue('J'.$key1, $value['sintroduce'])
                ->setCellValue('K'.$key1, $value['ts'])
                ->setCellValue('L'.$key1, $value['ks'])
                ->setCellValue('M'.$key1, $value['yj'])
                ->setCellValue('N'.$key1, $value['material'])
                ->setCellValue('O'.$key1, date('Y-m-d',$value['time']))
                ->setCellValue('P'.$key1, $value['remarks'])
                ->setCellValue('Q'.$key1, '0')
                ->setCellValue('R'.$key1, '0');
                
            }else{
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$key1, $value['num'])
                ->setCellValue('B'.$key1, $value['user'])
                ->setCellValue('C'.$key1, $value['zman'])
                ->setCellValue('D'.$key1, $value['author'])
                ->setCellValue('E'.$key1, $value['name'])
                ->setCellValue('F'.$key1, $value['size'])
                ->setCellValue('G'.$key1, $value['type'])
                ->setCellValue('H'.$key1, $value['wtr'])
                ->setCellValue('I'.$key1, $value['years'])
                ->setCellValue('J'.$key1, $value['sintroduce'])
                ->setCellValue('K'.$key1, $value['ts'])
                ->setCellValue('L'.$key1, $value['ks'])
                ->setCellValue('M'.$key1, $value['yj'])
                ->setCellValue('N'.$key1, $value['material'])
                ->setCellValue('O'.$key1, date('Y-m-d',$value['time']))
                ->setCellValue('P'.$key1, $value['remarks'])
                ->setCellValue('Q'.$key1, date('Y-m-d',$value['lsrtime']))
                ->setCellValue('R'.$key1, date('Y-m-d',$value['lsctime']));
            }
            
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' .'.xlsx"');
        header('Cache-Control: max-age=0');
        
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
        
        }
}