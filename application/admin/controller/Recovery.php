<?php
namespace app\admin\controller;

use Symfony\Component\Yaml\Tests\DumperTest;
use think\Controller;
use think\Request;
use think\Session;
use think\Db;
use app\admin\controller\Common;
class Recovery extends Common
{
    public function recovery()
    {
        if (request()->isGet()) {
            $num = input('get.num');

            $data['num'] = $num;
            if (empty($num)) {

            } else {
                $data['isdelete'] = 1;
                $data['isnew']=1;
                
                $list = Db::table('hd_connection')//该商品的所有数据及操作
                ->alias('a')
                    ->join('hd_state w', 'a.id = w.cid')
                    ->where($data)
                    ->select();

                $count=count($list);
                $pageid=input('pageid',1,'int');
                //每页10条数据
                $per_page =20;
                //当前页
                $page['nowpage']=$pageid;
                //上一页
                $page['prev']=$pageid-1;
                //下一页
                $page['next'] = $pageid+1;
                //总页数
                $page['total'] = ceil($count/$per_page);
                //判断页码范围
                if($page['prev']<=0){
                    $page['prev']=1;
                }
                if($page['next']>=$page['total']){
                    $page['next']=$page['total'];
                }

                $list = Db::table('hd_connection')//该商品的所有数据及操作
                ->alias('a')
                    ->join('hd_state w', 'a.id = w.cid')
                    ->where($data)
                    ->page($pageid)
                    ->limit($per_page)
                    ->select();

                foreach ($list as $k => $v) {
                    $img = $v['image'];
                    $a = explode(',', $img);
                    $list[$k]['image1']=$a;
                    $stt='_';
                    $a=substr_replace($a,$stt,9,0);
                    $list[$k]['image2'] = $a;
                }
              //  dump($list);die;
                $this->assign('abclist', $list);
                $this->assign('page',$page);
                return view();
            }
        }

        $aclist = Db::table('hd_connection')//该商品的所有数据及操作
        ->alias('a')
            ->join('hd_state w', 'a.id = w.cid')
            ->where(['isdelete'=>1,'isnew'=>1])
            ->select();

        $count=count($aclist);
        $pageid=input('pageid',1,'int');
        //每页10条数据
        $per_page =20;
        //当前页
        $page['nowpage']=$pageid;
        //上一页
        $page['prev']=$pageid-1;
        //下一页
        $page['next'] = $pageid+1;
        //总页数
        $page['total'] = ceil($count/$per_page);
        //判断页码范围
        if($page['prev']<=0){
            $page['prev']=1;
        }
        if($page['next']>=$page['total']){
            $page['next']=$page['total'];
        }
        $aclist=Db::table('hd_connection')                                 //该商品的所有数据及操作
        ->alias('a')
            ->join('hd_state w','a.id = w.cid')
            ->where(['isdelete'=>1,'isnew'=>1])
            ->page($pageid)
            ->limit($per_page)
            ->select();

        foreach ($aclist as $k => $v) {
            $img = $v['image'];
            $a = explode(',', $img);
            $aclist[$k]['image1']=$a;
            $stt='_';
            $a=substr_replace($a,$stt,9,0);
            $aclist[$k]['image2'] = $a;
        }



        $this->assign('page',$page);
        $this->assign('abclist', $aclist);

        

        return view();
    }

    public function huanyuan()
    {
        
        //还原
        $id = input('id');
       
       
        if (empty($id)) {
           $this->error('获取数据失败,请重试',url('Recovery/recpvery'));
        } else {
           
          $data=Db::table('hd_connection')->where(['id'=>$id])->update(['isdelete'=>0]);
          
            if ($data==1) {
                $this->success('还原成功',url('Recovery/recovery'));
            } else {
                $this->error('还原失败,请重试',url('Recovery/recovery'));
            }
            return $this->fetch('recovery');
        }

    }
}