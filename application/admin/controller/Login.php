<?php
namespace app\admin\controller;
use think\Controller;
use think\Request;
use think\Session;
use think\Db;

class Login extends Controller
{
    public function login(){

        return view();
    }

    //登录功能实现
    public function flogin(){
        if(request()->isPost())
        {
            $name=input('post.name');
            $psd=input('post.pswd');

            if(empty($name)||empty($psd)){
                echo "<script>alert('用户名，密码不能为空！');location.href='login.html'</script>";
            }else{
                $psd=jiami($psd);
                $array['name']=$name;
                $array['password']=$psd;
                $aatt=getadmininfo($array);
                if(!empty($aatt)){
                    Session::set('name',$name);

                    $this->redirect("Index/index");
                }else{
                    echo "<script>alert('用户名或密码错误！');location.href='login.html'</script>";
                }
            }
        }else{
            echo "<script>alert('404');location.href='login.html'</script>";

        }

    }

    public function login_v2(){
        return view();
    }
}