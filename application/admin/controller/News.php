<?php
namespace app\admin\controller;
use Symfony\Component\Yaml\Tests\DumperTest;
use think\Controller;
use think\Request;
use think\Session;
use think\Db;
use app\admin\controller\Common;

class News  extends Common
{
    public function newedit(){
        if(request()->isPost())
        {
            $data=input('post.');
            $sdata['state']=input('post.state');
            $conn=Db::table('hd_connection');
            $file = request()->file('fileAttach');
            $fname='';
            foreach($file as $f){
                $info = $f->validate(['size'=>1048576000,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
             //   dump($info);
                $home=$_SERVER['DOCUMENT_ROOT'];
                if($info){
                    $fname .= $info->getsaveName().',';

                    $savename = $info->getFilename();
                    $savenam = $info->getsaveName();
                    $aa=substr($savenam,0,9);
                    $newname12="_".$savename;
                    \think\Image::open($home.'/Public/uploads/'.$savenam)
                        ->thumb(750,600)->save($home.'/Public/uploads/'.$aa.$newname12);
                }
            }
            $fname=trim($fname,',');
            $wh['num']=$data['num'];
            $find=$conn->where(['num'=>$data['num']])->find();
            if($find!=null){
                echo "<script>alert('请勿重复上传！');location.href='newedit.html';</script>";
            }else{
                $data=input('post.');
                $sdata['time']=strtotime($data['time']);
                $data['time']=strtotime($data['time']);
                unset($data['state']);
                if(($data['lsrtime'])!=''){
                    $data['lsrtime']=strtotime($data['lsrtime']);
                }
                if(($data['lsctime'])!=''){
                    $data['lsctime']=strtotime($data['lsctime']);
                }


                $data['image']=$fname;
                $data['stime']=time();
                $a=Db::table('hd_connection')->insert($data);
                //获取商品id
                $tt=Db::table('hd_connection')->where($wh)->find();
                $id=$tt['id'];
                $sdata['cid']=$id;

                $b=Db::table('hd_state')->insert($sdata);
                if($a==1&&$b==1){
                    $this->success('添加藏品成功',url('Collect/collection'),2);
                }else{
                    $this->error('添加藏品失败，请重试',url('News/newedit'),2);
                }
            }

        }else{
            return view();
        }

    }
    public function newuser(){
        if(request()->isPost()){
            $name=input('post.name');
            $zname=input('post.zname');
            $section=input('post.department');
            $power=input('post.power');
            $data['name']=$name;
            $data['zname']=$zname;
            $data['section']=$section;
            $data['power']=$power;

            $away=Db::table('hd_admin');
            if(empty($name)||empty($zname)||empty($section)||empty($power)){
                echo "<script>alert('用户名，姓名，所属部门不能为空！');location.href='newuser.html'</script>";
            }else{
                $dd=$away->where('name',$name)->find();

               if($dd==''){
                   $aarr = Db::table('hd_admin')->insert($data);
                   if(!empty($aarr)){
                      $this->redirect('set/setting_user',2, '页面跳转中...');
                   } else {
                       echo "<script>alert('添加失败！请重新上传');location.href='newuser.html'</script>";
                   }
               }else{
                   $this->error('用户名重复',url('News/newuser'),2);
               }
            }
        }
        return view();
    }

    public function newschange(){
        if(request()->isPost){

        }else{
            $id=input('id');
            $con=Db::table('hd_connection')->where('id='.$id)->find();

            $state=Db::table('hd_state')->where('cid='.$id)->find();


            $data=array_merge($con,$state);

            $a=date('Y-m-d h:m:s',$data['time']);

            $data['time']=$a;
            if($data['lsrtime']!=''){
                $data['lsrtime']=date('Y-m-d h:m:s',$data['lsrtime']);
            }
            if($data['lsctime']!=''){
                $data['lsctime']=date('Y-m-d h:m:s',$data['lsctime']);
            }
            $a=explode(',', $data['image']);
        //    dump($a);die;
            foreach ($a as $k=>$v){
                $stt='_';
                $b=substr_replace($v,$stt,9,0);

                $a[$k]=$b;

            }

            $data['image']=$a;

         //  dump($data);die;
            $this->assign('data',$data);
            return $this->fetch();
        }
    }

    public function a(){
        if(request()->isPost()){
            $data=input('post.');
            $da['num']=$data['num'];
            $file = request()->file('fileAttach');
            $fame='';
            if(($data['lsrtime'])!=''){
                $data['lsrtime']=strtotime($data['lsrtime']);
            }
            if(($data['lsctime'])!=''){
                $data['lsctime']=strtotime($data['lsctime']);
            }

            $data['time']=strtotime($data['time']);
            $sid=Db::table('hd_connection')->field('id')->where($da)->find();
            $stime['time']=$data['time'];

           $stime1=Db::table('hd_state')->field('time')->where('cid='.$sid['id'])->update($stime);

            if(empty($file)){
                //图片为空，修改基本信息
                $aa=Db::table('hd_connection')->where($da)->update($data);

                if($aa==1){
                    $this->success('编辑藏品成功',url('Collect/collection'),2);
                }else{
                    $this->error('编辑藏品失败,请重试',url('Collect/collection'),2);
                }
            }else if(!empty($file)){
                //图片不为空，信息一起修改
                foreach($file as $f){
                    $info = $f->validate(['size'=>1048576000,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
                    //   dump($info);
                    $home=$_SERVER['DOCUMENT_ROOT'];
                    if($info){
                        $fame .= $info->getsaveName().',';

                        $savename = $info->getFilename();
                        $savenam = $info->getsaveName();
                        $aa=substr($savenam,0,9);
                        $newname12="_".$savename;
                        \think\Image::open($home.'/Public/uploads/'.$savenam)
                            ->thumb(750,600)->save($home.'/Public/uploads/'.$aa.$newname12);
                    }
                }

            //    dump($data);die;

              /*  $data['time']=strtotime($data['time']);
                unset($data['state']);
                if(($data['lsrtime'])!=''){
                    $data['lsrtime']=strtotime($data['lsrtime']);
                }
                if(($data['lsctime'])!=''){
                    $data['lsctime']=strtotime($data['lsctime']);
                }*/

             //   $sdata['time']=time();
                $data['image']=$fame;
                $aa=Db::table('hd_connection')->where($da)->update($data);

            //    $aa=Db::table('hd_state')->where($da)->update($sdata);
                if(!empty($aa)){
                    $this->success('编辑藏品成功',url('Collect/collection'),2);
                }else{
                    $this->error('编辑藏品失败！请重试',url('Collect/collection'),2);
                }
            }else{
                $this->error('编辑藏品失败，请重试',url('Collect/collection'),2);
            }

        }

    }

}