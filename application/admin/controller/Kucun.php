<?php
namespace app\admin\controller;
use think\Controller;
use think\Request;
use think\Session;
use think\Db;
use app\admin\controller\Common;

class Kucun extends Common
{
    public function addcollection(){
        return view();
    }
    public function outcollection(){
        $id=input('id');
        
        $str=session::get('name');
        if(request()->isPost()){
            $data=input('post.');
        
           
            if(empty($data['num'])&&empty($data['zname'])&&empty($data['why'])){
                echo "<script>alert('为空！');location.href='outcollection.html';</script>";
            }else{
                                                                                       //根据单号查找id
                $daa['id']=$data['id'];
                $wh['zname']=$str;
                $a=Db::table('hd_connection')->where($daa)->find();
                $aa=Db::table('hd_admin')->where($wh)->find();
                $cid=$a['id'];                                                             //藏品id
                $aid=$aa['id'];                                                            //出库人id
                if(empty($aid)&&empty($cid)){
                    echo "<script>alert('出库失败！');location.href='outcollection.html';</script>";
                }else{
                    //获取当前数据
                    $dat['cid']=$cid;
                    $dat['aid']=$aid;
                    $dat['state']=1;
                   
                    $dat['why']=$data['why'];
                    $dat['time']=strtotime($data['time']);
                    $dat['zname']=$data['zname'];
                   
                    //  dump($dat);die;
                   
                    //改变原来状态
                    Db::table('hd_state')->where('cid='.$dat['cid'])->update(['isnew'=>0]);
                    //插入新状态
                    $b=Db::table('hd_state')->insert($dat);
                    
                   /*  $c=Db:table('hd_state')->save($datan)->where('cid='.$dat['cid'].' and id!='); */
                    if($b==1){
                        $this->success('出库成功',url('kucun/storehouse'));
                    }else{
                        $this->error('出库失败,请重试',url('kucun/storehouse'));
                    }
                }
            }
        }
       
        $alist=Db::table('hd_connection')                                 //该商品的所有数据及操作
            ->alias('a')
            ->join('hd_state w','a.id = w.cid')
            ->where(['a.id'=>$id])
            ->find();
       
        $this->assign('alist',$alist);

      //  $this->assign('num',$num);
    
        return view();
    }


    public function storehouse(){
            $num1=trim(input('get.num'));
            $num='%'.$num1.'%';
          
           // dump($num);die;
           
             
              
                
               
                $list=Db::table('hd_connection')
                ->alias('a')
                ->join('hd_state b','a.id=b.cid')
                ->where(['num'=>['like',$num],'b.state'=>0,'b.isnew'=>1])
               
                ->paginate(10,false,['query'=>request()->param()]);
               
                
                $this->assign('list',$list);
                
                return $this->fetch();
               
           
       
       
    }


    //归还藏品操作
    public function guihuan(){
        $num1=trim(input('get.num'));
        $num='%'.$num1.'%';
       
           

            $abclist = Db::table('hd_connection')//该商品的所有数据及操作
            ->alias('a')
                ->join('hd_state w', 'a.id = w.cid')
                ->where(['num'=>['like',$num],'isnew'=>1,'state'=>2])
               
                ->paginate(10,false,['query'=>request()->param()]);
            
          
            $this->assign('abclist', $abclist);
          
            return view();

    }
    
    //出入库明细操作
    public function mingxi(){
        if(request()->isGet()){
           $num=trim(input('get.num'));
           
       
              
                $list_page=Db::table('hd_connection')                                 //该商品的所有数据及操作
                ->alias('a')
                    ->join('hd_state w','a.id = w.cid')
                    ->where('num','like','%'.$num.'%')
                    ->order('a.id desc')
                    ->paginate(10,false,['query'=>request()->param()]);
                $list=$list_page->toArray();
                    
                foreach ($list['data'] as $k => $v) {
                    $img = $v['image'];
                    $a = explode(',', $img);
                    $list[$k]['image1']=$a;
                    $stt='_';
                    $a=substr_replace($a,$stt,9,0);
                    $list['data'][$k]['image2'] = $a;
                }


                $this->assign('aclist',$list['data']);
                $this->assign('list',$list_page->render());
                return view();
           
        }
        
       
    }
    
    
    public function return1(){

           $id['Id']=input('id');//获取get方式传递过来的id
           $id['isnew']=1;
           $data1=Db::table('hd_state')->where($id)->find();//获取当前id下的状态信息

        $data['time']=time();
        $data['state']=0;
        $data['cid']=$data1['cid'];
        $data['aid']=$data1['aid'];
        $data['rid']=$data1['rid'];
        $data['why']=$data1['why'];
        $data['zname']=$data1['zname'];

        $old=Db::table('hd_state')->where('Id='.$data1['Id'])->update(['isnew'=>'0']);
          // $updata=Db::table('hd_state')->where('id='.$id)->update(['state'=>0,'isnew'=>0]);//改变原来状态
        
          /*  $sql=Db::table('hd_state')->where($data)->find();//根据新状态数据查找id */
           
           
          $insert=Db::table('hd_state')->insert($data);//添加新的状态数据*/
          /*  Db::table('hd_state')->where('cid='.$data['cid'].' and id <>'.$sql['id'])->update(['isnew'=>0]);//改变当前藏品下其他状态的状态值为0,即改变为上一步操作 */
           if($insert==1){
                $this->success('归还成功',url('kucun/guihuan'));
           }else{
                $this->error('归还失败,请重试',url('kucun/guihuan'));
           }

        
    }
    
    
    
    public function rollout(){
        
       
       $num1=trim(input('get.num')); 
       $num='%'.$num1.'%';
       
      
        $a1=Db::table('hd_connection')
        ->alias('a')
        ->join('hd_state b','a.id=b.cid')
        ->where(['num'=>['like',$num],'b.state'=>0,'b.isnew'=>1])
        ->order('a.id desc')
        ->paginate(10,false,['query'=>request()->param()]);
        
         
     
        $this->assign('list',$a1);
        
        return $this->fetch();
        
        
    }
    
    public function loan(){
        $id=input('id');
        
        $str=session::get('name');
        if(request()->isPost()){
            $data=input('post.');
        
             
            if(empty($data['num'])&&empty($data['zname'])&&empty($data['why'])){
                echo "<script>alert('为空！');location.href='outcollection.html';</script>";
            }else{
                //根据单号查找id
                $daa['id']=$data['id'];
                $wh['zname']=$str;
                $a=Db::table('hd_connection')->where($daa)->find();
                $aa=Db::table('hd_admin')->where($wh)->find();
                $cid=$a['id'];                                                             //藏品id
                $aid=$aa['id'];                                                            //出库人id
                if(empty($aid)&&empty($cid)){
                    echo "<script>alert('转库失败！');location.href='outcollection.html';</script>";
                }else{
                    //获取当前数据
                    $dat['cid']=$cid;
                    $dat['aid']=$aid;
                    $dat['state']=2;
                     
                    $dat['why']=$data['why'];
                    $dat['time']=strtotime($data['time']);
                    $dat['zname']=$data['zname'];
                     
                    //  dump($dat);die;
                     
                    //改变原来状态
                    Db::table('hd_state')->where('cid='.$dat['cid'])->update(['isnew'=>0]);
                    //插入新状态
                    $b=Db::table('hd_state')->insert($dat);
        
                    /*  $c=Db:table('hd_state')->save($datan)->where('cid='.$dat['cid'].' and id!='); */
                    if($b==1){
                        $this->success('转库成功',url('kucun/rollout'));
                    }else{
                        $this->error('转库失败,请重试',url('kucun/rollout'));
                    }
                }
            }
        }
        
        $alist=Db::table('hd_connection')                                 //该商品的所有数据及操作
        ->alias('a')
        ->join('hd_state w','a.id = w.cid')
        ->where(['a.id'=>$id])
        ->find();
         
        $this->assign('alist',$alist);
       
        //  $this->assign('num',$num);
        
        return $this->fetch();
        
        
        
        
    }
    
}