<?php
namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Session;
use think\Db;

class Index extends Controller                                      //首页的显示
{
    public function index()
    {
        $str=session::get('name');
        if(empty($str)){
            echo "<script>location.href='index/Index/login'</script>";
        }else{
            $away=Db::table('hd_connection');

            $date=input('post.');
            if(empty($date)){
                $data=getselectinfor($away,$date);  //查询条件$date为空时的查询
            }else{
                $data=getselectinfor($away,$date);
            }
            $this->assign('data',$data);
            return view();
        }
    }


//藏品基本功能上传
    public function aaa(){
        if(request()->isPost())
        {
            $data=input('post.');
            $away=Db::table('hd_connection')
                ->alias('a')
                ->join('hd_comment w','a.id = w.cid');
            if(empty($data['name'])||empty($data['size'])){
                echo "<script>alert('不能为空！');location.href='aaa.html'</script>";
            }else{
                $ttsa=getfindinfor($away,$data);
                if(!empty($ttsa)){
                    echo "<script>alert('请勿重复上传！');location.href='aaa.html'</script>";
                }else {
                    $aarr = getinsertinfo($away, $data);
                    if (!empty($aarr)) {
                        echo "<script>alert('上传成功！');</script>";
                    } else {
                        echo "<script>alert('上传失败！请重新上传');location.href='aaa.html'</script>";
                    }
                }
            }
        }else{

        }
        return view();
    }





//登录页面
    public function login()
    {
        return view();
    }

//登录功能实现
    public function flogin(){
        if(request()->isPost())
        {
            $name=input('post.name');
            $psd=input('post.pswd');
            if(empty($name)||empty($psd)){
                echo "<script>alert('用户名，密码不能为空！');location.href='login.html'</script>";
            }else{
                $psd=jiami($psd);
                $array['name']=$name;
                $array['password']=$psd;
                $aatt=getadmininfo($array);
                if(!empty($aatt)){
                   // $round = rand(1,10000);
                 //   session_start();

                    Session::set('name',$name);
                 //   Session::set('rand','thinkphp');
                    $this->redirect("Index/index");

                    //echo "<script>alert('登录成功！');window.location.href='index.html'</script>";
                  //  echo "<script>location.href='index/Index/index'</script>";
                  //  $url="http://www.jb51.net";
                  //   echo "<SCRIPT LANGUAGE=\"JavaScript\">location.href='$url'</SCRIPT>";
                }else{
                    echo "<script>alert('用户名或密码错误！');location.href='login.html'</script>";
                }
            }
        }else{
            echo "<script>alert('404');location.href='login.html'</script>";

        }

    }





//ajax功能
    public function dlogin(){
        if(Request::instance()->isAjax()){
            $name=Request::instance()->filter(['name','htmlspecialchars']);
            $pswd=Request::instance()->filter(['password','htmlspecialchars']);
            var_dump($name);
            var_dump($pswd);

            if(!empty($name)||!empty($pswd)){
                $data[]='用户名和密码不能为空';
                echo json_encode($data);
            }else{
                $psd=jiami($pswd);
                $array['name']=$name;
                $array['password']=$psd;
                $aatt=getadmininfo($array);
                if(!empty($aatt)){
                    $round = rand(1,10000);
                    session_start();
                    session('token',$round,10);
                    session('name',$name,10);
                    $data[] = 1;
                    echo json_encode($data);
                }else{
                    $data[] = "登录失败";
                    echo json_encode($data);
                }
            }
        }
        else{
            $data[] = 'AJAX请求失败！';
            echo json_encode($data);
        }
    }
}
